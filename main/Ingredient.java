package main;

class Ingredient {
    String nomAliment;
    String etat;
    int quantite;
    String unite;

    Ingredient(String n, String e, int q, String unite) {
        this.nomAliment = n;
        this.etat = e;
        this.quantite = q;
        this.unite = unite;
    }
}
